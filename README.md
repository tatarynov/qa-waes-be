# WAES Assignment for QA specialists

Thank you for your interest in WAES and for taking the time to complete QA challenge.  
**We love to push techology further to change people's lives for the better.**  
...and Quality Assurance is an important part of this change.

## Goal

The goal of this assignment is to check your skills in backend testing.  
We evaluate the assignment depending on your level of seniority regarding coverage, strategy and presentation.

## Task

Just imagine you work on a new platform that needs its functionality to be tested.  
This platform has a REST backend implementation that deals with:  

- creating accounts;
- logging in;
- updating accounts;
- deleting accounts;
- authorization (basic admin rights).

To start the backend REST service you should:

- Clone project ```https://bitbucket.org/waesworks/backend-for-testers.git```
- Follow instructions that are described in the ```README.md``` file

Your tasks are:

1. Create an automation project from scratch for testing the BE application.
2. Automate tests for the following features using the appropriate endpoints:
	- Sign up 
	- Login
	- Update user info 
	- Delete user

## Additional requirements

- Your project should cover as many functional scenarios as you judge necessary for the given features and the test data available.
- Your project should be scalable for future features.
- Do not change original BE application

## Technologies

The assignment can be developed with any of the three most popular languages (Java/JS/Python) and any testing framework/library.  
Choose your weapon of choice.  

## Submission Guidelines

- Cover functional scenarios, and don't forget about some corner cases.
- Report with test results should be automatically generated.
- Include a `README.md` with instructions how to run tests and where to find the report, together with any additional information you consider appropriate (assumptions, design decisions made, etc.)

## Nice to have

- Flexibility regarding the server that the test project is running against.
- If defects are found, a report with all relevant information.
- Suggestions for improvements.



#### Please upload the assignment to your personal GitHub/Bitbucket account once finished and send the link to the responsible WAES sourcer before deadline.